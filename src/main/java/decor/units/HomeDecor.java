package decor.units;
/**
 * Class representing types of decorations for home
 * @version 0.1
 * @author Andriy Mota
 * @since 2019.11.10
 */
public class HomeDecor extends DecorType {
    /**
     * type of decoration
     */
    private String type = "Home_decoration";
    /**
     * Priority, which is used to sort elements is List<br>
     * Max priority = 1, min depends on value of decorTypes<br>
     * In my case min = 3
     */
    public int priority;

    /**
     * @param i sets priority of DecorType
     */
    public HomeDecor(final int i) {
        super(i);
        priority = i;
    }
    /**
     * @return String representation of HomeDecor priority
     */
    public String toString() {
        return type;
    }
}
