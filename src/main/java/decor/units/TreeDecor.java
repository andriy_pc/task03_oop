package decor.units;
/**
 * Class representing types of decorations for tree
 * @version 0.1
 * @author Andriy Mota
 * @since 2019.11.10
 */
public class TreeDecor extends DecorType {
    /**
     * type of decoration
     */
    private String type = "Tree_decoration";
    /**
     * Priority, which is used to sort elements is List<br>
     * Max priority = 1, min depends on value of decorTypes<br>
     * In my case min = 3
     */
    private int priority;
    /**
     * @param i sets priority of DecorType
     */
    public TreeDecor(final int i) {
        super(i);
        priority = i;
    }
    /**
     * @return String representation of TreeDecor priority
     */
    public String toString() {
        return type;
    }
}
