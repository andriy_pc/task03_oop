package data.units;
import java.util.Random;
import decor.units.*;

/**
 * Class to generate Decoration Objects
 * @version 0.1
 * @author Andriy Mota
 * @since 2019.11.10
 */
public class DecorationGenerator {
    /**
     * Random is used to randomly generate Decorations
     */
    private Random rand = new Random(47);
    /**
     * Store decoration types
     */
    private DecorType[] dTypes = new DecorType[] {
            new CarDecor(1), new HomeDecor(2), new TreeDecor(3)
    };
    /**
     * Store locations of decorations to use
     */
    private String[] locations = new String[] {
            "car", "home", "tree"
    };
    /**
     * Store possible colors of decorations
     */
    private String[] colors = new String[] {
            "red", "green", "blue"
    };
    /**
     * Generate randomly new Decoration object
     *
     * @return randomly generated Decoration object
     */
    public Decoration generateDecor() {
        int dt = rand.nextInt(3);
        int clr = rand.nextInt(3);

        switch (dt) {
            case 0: {
                return new Ball(colors[clr], locations[dt], dTypes[dt]);
            }
            case 1: {
                return new Candy(colors[clr], locations[dt], dTypes[dt]);
            }
            case 2: {
                return new Gerland(colors[clr], locations[dt], dTypes[dt]);
            }
            default : {
                return null;
            }
        }
    }
}
