package data.units;
import decor.units.DecorType;

/**
 * Abstract class representing New-Year Decoration<br>
 * To provide ability to use Decorations in List, and use sort method,<br>
 * Decoration implements Comparable. Only field DecorType is valid in<br>
 * comparing.<br>
 * @version 0.1
 * @author Andriy Mota
 * @since 2019.11.10
 */
public abstract class Decoration implements Comparable<Decoration> {
    /**
     * Color of the decoration
     */
    private String color;
    /**
     * Location of the decoration
     */
    private String location;
    /**
     * Type of decoration
     */
    private DecorType decorType;
    /**
     * @param color set color for decoration
     * @param location set location for decoration
     * @param decorType set decorType for decoration
     */
    public Decoration(final String color, final String location,
                      final DecorType decorType)     {
        this.color = color;
        this.location = location;
        this.decorType = decorType;
    }
    /**
     * To provide ability to use Decorations in List, and use sort method<br>
     * Decoration implements Comparable. This method redirect call to only<br>
     * one valid object DecorType.
     *
     * @param o Decoration object to compare with
     * @return 0 if original object has bigger priority than o;<br>
     * -1 if original object has bigger priority than o
     */
    public int compareTo(final Decoration o) {
        return decorType.compareTo(o.decorType);
    }
    /**
     * To provide nice representation of Decoration object in console
     *
     * @return String representation of Decoration object
     */
    public String toString() {
        StringBuilder result = new StringBuilder(getClass().getSimpleName());
        result.append("\ncolor: ");
        result.append(color);
        result.append("\nlocation: ");
        result.append(location);
        result.append("\ndecorType: ");
        result.append(decorType);
        result.append("\n----------------------------------------------");
        return result.toString();
    }
    /**
     * To easy compare Decoration objects, it is necessary to have<br>
     * their priority.
     *
     * @return priority of DecorType object
     */
    public int getDecorType() {
        return decorType.getPriority();
    }
}